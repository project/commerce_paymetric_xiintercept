/**
 * @file
 * Javascript to submit the form on payment success.
 */

(function ($, Drupal, drupalSettings) {

  'use strict';

  /**
   * Attaches the commercePaymetricXiInterceptIFrame behavior.
   *
   * @type {Drupal~behavior}
   *
   * @see Drupal.paymetricXiInterceptIFrame
   */
  Drupal.behaviors.commercePaymetricXiInterceptIFrame = {
    attach: function (context) {
      $(once('paymetric-xiintercept-iframe-form-processed', '.paymetric-xiintercept-iframe-form', context)).each(function () {
        // Get the form.
        var $paymentForm = $(this).closest('form');
        // Get the access token.
        var $accessToken = drupalSettings.paymetricXiInterceptIFrame.accessToken;

        // Resize the iframe.
        var iframe = document.getElementById(drupalSettings.paymetricXiInterceptIFrame.iframeId);
        if (iframe) {
          $(iframe).css('height', '300px');
          $(iframe).css('width', '400px');
        }

        // Submit the form on successful CC submission to Paymetric.
        $paymentForm.on('submit', function (event) {
          event.preventDefault();

          if (iframe) {
            $XIFrame.submit({
              iFrameId: drupalSettings.paymetricXiInterceptIFrame.iframeId,
              targetUrl: drupalSettings.paymetricXiInterceptIFrame.iframeUrl,
              onSuccess: function (msg) {
                var message = JSON.parse(msg);
                if (message && message.data.HasPassed) {
                  $('#paymetric-access-token', $paymentForm).val($accessToken);
                  $paymentForm.get(0).submit();
                }
                else {
                  paymetricXiInterceptErrorDisplay(Drupal.t('An error occurred while submitting the payment to the payment gateway. Please try again.'));
                }
              },
              onError: function (msg) {
                paymetricXiInterceptErrorDisplay(msg);
              },
            });
          }
        });

        /**
         * Helper for displaying the error messages within the form.
         */
        function paymetricXiInterceptErrorDisplay(error_message) {
          // Display the message error in the payment form.
          $paymentForm.find('#payment-errors').html(Drupal.theme('commercePaymetricXiInterceptError', error_message));
        }

        /**
         * @extends Drupal.theme.
         */
        $.extend(Drupal.theme, {
          commercePaymetricXiInterceptError: function (message) {
            return $('<div class="messages messages--error"></div>').html(message);
          }
        });
      });
    }
  }
})(jQuery, Drupal, drupalSettings);
