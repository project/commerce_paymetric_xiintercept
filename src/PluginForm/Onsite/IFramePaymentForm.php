<?php

namespace Drupal\commerce_paymetric_xiintercept\PluginForm\Onsite;

use Drupal\commerce\InlineFormManager;
use Drupal\commerce_payment\PluginForm\PaymentMethodAddForm;
use Drupal\commerce_paymetric_xiintercept\Api\IFrameClientInterface;
use Drupal\commerce_store\CurrentStoreInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides onsite payment form via Paymetric XiIntercept's iFrame integration.
 */
class IFramePaymentForm extends PaymentMethodAddForm {

  /**
   * The Paymetric XiIntercept IFrame API client.
   *
   * @var \Drupal\commerce_paymetric_xiintercept\Api\IFrameClient
   */
  protected $apiClient;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a new IFramePaymentForm object.
   *
   * @param \Drupal\commerce_store\CurrentStoreInterface $current_store
   *   The current store.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerce\InlineFormManager $inline_form_manager
   *   The inline form manager.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   * @param \Drupal\commerce_paymetric_xiintercept\Api\IFrameClientInterface $api_client
   *   The Paymetric XiIntercept IFrame API client.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   */
  public function __construct(CurrentStoreInterface $current_store, EntityTypeManagerInterface $entity_type_manager, InlineFormManager $inline_form_manager, LoggerInterface $logger, IFrameClientInterface $api_client, MessengerInterface $messenger) {
    parent::__construct(
      $current_store,
      $entity_type_manager,
      $inline_form_manager,
      $logger
    );

    $this->apiClient = $api_client;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('commerce_store.current_store'),
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_inline_form'),
      $container->get('logger.channel.commerce_paymetric_xiintercept'),
      $container->get('commerce_paymetric_xiintercept.api_client'),
      $container->get('messenger'),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function buildCreditCardForm(array $element, FormStateInterface $form_state) {
    // To display validation errors.
    $element['payment_errors'] = [
      '#type' => 'markup',
      '#markup' => '<div id="payment-errors"></div>',
      '#weight' => -200,
    ];

    // Generate an access token from Paymetric to initiate a XiIntercept
    // session.
    if (empty($form_state->getValue('paymetric_access_token'))) {
      $plugin = $this->entity->getPaymentGateway()->getPlugin();
      $config = $plugin->getConfiguration();
      $access_token = $this->apiClient->createAccessToken($config);
    }
    else {
      $access_token = $form_state->getValue('paymetric_access_token');
    }
    $element['paymetric_access_token'] = [
      '#type' => 'hidden',
      '#attributes' => [
        'id' => 'paymetric-access-token',
      ],
    ];

    // Embed the iFrame template xml needed for Paymetric to generate the
    // credit card fields.
    $base_url = $config['mode'] === 'test' ? IFrameClientInterface::IFRAME_BASE_URL_DEV : IFrameClientInterface::IFRAME_BASE_URL_PROD;
    $iframe_url = $base_url . '/' . $config['merchant_guid'] . '/' . $access_token . '/True';
    $iframe_id = 'commerce-paymetric-xiintercept-iframe';
    $element['iframe_template_xml'] = [
      '#type' => 'inline_template',
      '#template' => '<iframe id="{{ iframe_id }}" src="{{ url }}"></iframe>',
      '#context' => [
        'iframe_id' => $iframe_id,
        'url' => $iframe_url,
      ],
    ];

    // Include the necessary libraries.
    $element['#attributes']['class'][] = 'paymetric-xiintercept-iframe-form';
    $element['#attached']['drupalSettings']['paymetricXiInterceptIFrame'] = [
      'iframeId' => $iframe_id,
      'iframeUrl' => $iframe_url,
      'accessToken' => $access_token,
    ];
    $element['#attached']['library'][] = 'commerce_paymetric_xiintercept/paymetric_xiintercept_iframe';
    $element['#attached']['library'][] = 'commerce_paymetric_xiintercept/iframe_form';

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  protected function validateCreditCardForm(
    array &$element,
    FormStateInterface $form_state
  ) {
    // The JS library performs its own validation.
  }

  /**
   * {@inheritdoc}
   */
  protected function submitCreditCardForm(
    array $element,
    FormStateInterface $form_state
  ) {
    // The payment gateway plugin will process the submitted payment details.
  }

}
