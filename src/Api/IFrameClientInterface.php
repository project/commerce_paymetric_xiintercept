<?php

namespace Drupal\commerce_paymetric_xiintercept\Api;

/**
 * Defines the interface for the iFrame API client.
 */
interface IFrameClientInterface {

  const SESSION_REQUEST_TYPE = 1;
  const ACCESS_TOKEN_URL_DEV = 'https://cert-xiecomm.paymetric.com/DIeComm/AccessToken';
  const ACCESS_TOKEN_URL_PROD = 'https://xiecomm.paymetric.com/DIeComm/AccessToken';
  const IFRAME_BASE_URL_DEV = 'https://cert-xiecomm.paymetric.com/DIeComm/View/Iframe';
  const IFRAME_BASE_URL_PROD = 'https://xiecomm.paymetric.com/DIeComm/View/Iframe';
  const IFRAME_RESPONSE_PACKET_URL_DEV = 'https://cert-xiecomm.paymetric.com/DIeComm/ResponsePacket';
  const IFRAME_RESPONSE_PACKET_URL_PROD = 'https://xiecomm.paymetric.com/DIeComm/ResponsePacket';
  const XIPAY_AUTHORIZATION_URL_PROD = 'https://xipayapi.paymetric.com/PMXIGGE/XiPay30WS.asmx';
  const XIPAY_AUTHORIZATION_URL_DEV = 'https://cert-xipayapi.paymetric.com/PMXIGGE/XiPay30WS.asmx';

  /**
   * Creates and returns an access token from Paymetric XiIntercept.
   *
   * @param array $config
   *   The Paymetric XiIntercept payment method config.
   *
   * @return string
   *   The unique token from Paymetric.
   */
  public function createAccessToken(array $config) : string;

  /**
   * Tokenizes the payment in Paymetric and fetches the tokenized card.
   *
   * @param array $config
   *   The Paymetric XiIntercept payment method config.
   * @param string $access_token
   *   The access token retrieved from `createAccessToken()`.
   *
   * @return array
   *   The credit card details.
   *   The original response is an XML packet that looks like this:
   *   `
   *   [
   *     'PaymetricResponse' => [
   *       'Fields' => [
   *         'FormField' => [
   *           0 => [
   *             'Name' => 'Card Type',
   *             'Value' => 'vi',
   *             'IsToTokenize' => 'false',
   *           ],
   *           1 => [
   *             'Name' => 'Expiration Month',
   *             'Value' => '2',
   *             'IsToTokenize' => 'false',
   *           ],
   *           2 => [
   *            'Name' => 'Expiration Year',
   *            'Value' => '2024',
   *            'IsToTokenize' => 'false',
   *           ],
   *           3 => [
   *             'Name' => 'Card Number',
   *             'Value' => '-E803-1111-RDWAVE6WM0XX7T',
   *             'IsToTokenize' => 'true',
   *           ],
   *           4 => [
   *             'Name' => 'Card Security Code',
   *             'Value' => '123',
   *             'IsToTokenize' => 'false',
   *           ],
   *           5 => [
   *            'Name' => 'Card Holder Name',
   *            'Value' => 'John Smith',
   *            'IsToTokenize' => 'false',
   *           ],
   *         ],
   *       ],
   *     ],
   *   ];
   *   `
   *   The packet is parsed and reordered so that only the `FormField` array
   *   values are returned and the index keys have the `Name` value as the key.
   *   So, the return array looks like this:
   *   `
   *   [
   *     'Card Type' => [
   *       'Name' => 'Card Type',
   *       'Value' => 'vi',
   *       'IsToTokenize' => 'false',
   *     ],
   *     'Expiration Month => [
   *       'Name' => 'Expiration Month',
   *       'Value' => '2',
   *       'IsToTokenize' => 'false',
   *     ],
   *     'Expiration Year => [
   *       'Name' => 'Expiration Year',
   *       'Value' => '2024',
   *       'IsToTokenize' => 'false',
   *     ],
   *     'Card Number' => [
   *       'Name' => 'Card Number',
   *       'Value' => '-E803-1111-RDWAVE6WM0XX7T',
   *       'IsToTokenize' => 'true',
   *     ],
   *     'Card Security Code' => [
   *       'Name' => 'Card Security Code',
   *       'Value' => '123',
   *       'IsToTokenize' => 'false',
   *     ],
   *     'Card Holder Name' => [
   *       'Name' => 'Card Holder Name',
   *       'Value' => 'John Smith',
   *       'IsToTokenize' => 'false',
   *     ],
   *   ],
   *   `
   */
  public function getResponsePacket(array $config, string $access_token) : array;

  /**
   * Authorizes the payment using the XiPay web service.
   *
   * @param array $config
   *   The Paymetric XiIntercept payment method config.
   * @param array $transaction_data
   *   An array of transaction data for the card to pass to XiPay.
   *
   * @return array
   *   The response array.
   */
  public function authorizePayment(array $config, array $transaction_data) : array;

}
