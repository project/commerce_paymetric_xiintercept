<?php

namespace Drupal\commerce_paymetric_xiintercept\Api;

use Drupal\commerce_payment\Exception\HardDeclineException;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use GuzzleHttp\ClientInterface;
use Psr\Log\LoggerInterface;

/**
 * Client for making requests to the Paymetric XiIntercept iFrame API.
 */
class IFrameClient implements IFrameClientInterface {

  /**
   * The HTTP client to fetch the access token.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Creates a new IFrameClient object.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The Guzzle HTTP client.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   */
  public function __construct(ClientInterface $http_client, LoggerInterface $logger) {
    $this->httpClient = $http_client;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public function createAccessToken(array $config): string {
    try {
      $host = $config['mode'] === 'test' ? self::ACCESS_TOKEN_URL_DEV : self::ACCESS_TOKEN_URL_PROD;
      $options = [
        'form_params' => [
          'MerchantGuid' => $config['merchant_guid'],
          'SessionRequestType' => self::SESSION_REQUEST_TYPE,
          'Signature' => base64_encode(hash_hmac('sha256', $this->getIframePacketXml($config), $config['shared_key'], TRUE)),
          'MerchantDevelopmentEnvironment' => 'php',
          'Packet' => $this->getIframePacketXml($config),
        ],
        'headers' => [
          'Accept' => 'application/json',
          'Content-Type' => 'application/x-www-form-urlencoded',
        ],
      ];
      $response = $this->httpClient->request('POST', $host, $options);
      $data = $response->getBody()->getContents();
      $encode_data = json_encode(simplexml_load_string($data));
      $decoded_data = json_decode($encode_data, TRUE);
      return $decoded_data['ResponsePacket']['AccessToken'];
    }
    catch (\Exception $e) {
      throw new PaymentGatewayException($e->getMessage());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getResponsePacket(array $config, $access_token): array {
    try {
      $host = $config['mode'] === 'test' ? self::IFRAME_RESPONSE_PACKET_URL_DEV : self::IFRAME_RESPONSE_PACKET_URL_PROD;
      $options = [
        'query' => [
          'MerchantGuid' => $config['merchant_guid'],
          'Signature' => base64_encode(hash_hmac('sha256', $access_token, $config['shared_key'], TRUE)),
          'AccessToken' => $access_token,
        ],
        'headers' => [
          'Accept' => 'application/json',
          'Content-Type' => 'application/x-www-form-urlencoded',
        ],
      ];
      $response = $this->httpClient->request('GET', $host, $options);
      $data = $response->getBody()->getContents();
      $encode_data = json_encode(simplexml_load_string($data));
      $decoded_data = json_decode($encode_data, TRUE);
      if (isset($decoded_data['Fields']['FormField'])) {
        return $this->reOrderResponsePacket($decoded_data['Fields']['FormField']);
      }
      else {
        throw new PaymentGatewayException('An error occurred while processing the payment.');
      }
    }
    catch (\Exception $e) {
      throw new PaymentGatewayException($e->getMessage());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function authorizePayment(array $config, array $transaction_data): array {
    try {
      $host = $config['mode'] === 'test' ? self::XIPAY_AUTHORIZATION_URL_DEV : self::XIPAY_AUTHORIZATION_URL_PROD;
      $authorization_xml = $this->getAuthorizationXml($config, $transaction_data);
      $options = [
        'body' => $authorization_xml,
        'headers' => [
          "Content-Type" => "text/xml; charset=utf-8",
          'SOAPAction' => 'Paymetric/XiPaySoap30/action/XiGGE.SoapOp',
        ],
      ];
      $response = $this->httpClient->request('POST', $host, $options);
      $data = $response->getBody()->getContents();
      // As the data is SOAP xml, clean up the Soap characters like carets, and
      // colons so that it can be converted to XML and then, converted to json
      // and then, into a PHP array.
      $data = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $data);
      $xml = new \SimpleXMLElement($data);
      $body = $xml->xpath('//sBody')[0];
      $encode_data = json_encode((array) $body);
      $decoded_data = json_decode($encode_data, TRUE);
      if ($decoded_data['SoapOpResponse']['SoapOpResult']['xipayvbresult']) {
        // Only send back keys with values.
        return array_filter($decoded_data['SoapOpResponse']['SoapOpResult']['packets']['ITransactionHeader']);
      }
      else {
        throw new HardDeclineException('Unable to verify the payment method details: ' . $decoded_data['SoapOpResponse']['SoapOpResult']['packets']['ITransactionHeader']['message']);
      }
    }
    catch (\Exception $e) {
      throw new PaymentGatewayException($e->getMessage());
    }
  }

  /**
   * Return the packet xml string that should be included in access token call.
   *
   * @param array $config
   *   The config array.
   *
   * @return string
   *   The xml string.
   */
  protected function getIframePacketXml(array $config): string {
    global $base_url;

    return '<?xml version="1.0" encoding="UTF-8"?>
<merchantHtmlPacketModel xmlns="Paymetric:XiIntercept:MerchantHtmlPacketModel">
<iFramePacket>
<hostUri>' . $base_url . '</hostUri>
<cssUri>' . $base_url . '/modules/contrib/commerce_paymetric_xiintercept/css/IFrameStyleSheet.css</cssUri>
</iFramePacket>
<merchantHtml>
<htmlSection class="merchant_paycontent">
<cardDropdownSection>
<tag class="PaymentDetailHeader" name="div">Credit Card Payment</tag>
<tag name="div">
<label for="cardType" text="Card Type" />
<ddlCardType id="cd" default-text="Select a card type">
<items>' . $this->getPaymentTypesforPacketXml($config) . '</items>
</ddlCardType>
</tag>
<tag name="div">
<label for="cardNumber" text="Card Number" />
<tboxCardNumber mask-number="true" tokenize="true" />
<validationMsg class="valmsg" for="cardNumber" />
</tag>
<tag name="div">
<label for="cardholderName" text="Cardholder Name" />
<tboxCardHolderName />
<validationMsg class="valmsg" for="cardholderName" />
</tag>
<tag name="div">
<label for="expDate" text="Expiration Date" />
<ddlExpMonth class="merchant_combos" default-text="Month" required="false" />
<ddlExpYear class="merchant_combos" default-text="Year" exp-date="true" required="false" years-to-display="10" />
<validationMsg class="valmsg" for="expYear" />
</tag>
<tag name="div">
<label for="cvv" text="Card CVV" />
<tboxCvv />
<validationMsg class="valmsg" for="cvv" />
</tag>
</cardDropdownSection>
</htmlSection>
</merchantHtml>
</merchantHtmlPacketModel>
';
  }

  /**
   * Construct the payment types xml for the Iframe based on the accepted cards.
   *
   * @param array $config
   *   The config array.
   *
   * @return string
   *   The payment types xml string.
   */
  protected function getPaymentTypesforPacketXml(array $config): string {
    $payment_type_xml = '';

    $accepted_credit_card_types = explode(', ', $config['accepted_credit_cards']);
    $credit_card_type_map = $this->creditCardTypeMap();
    foreach ($accepted_credit_card_types as $type) {
      $payment_type_xml .= '<item for="' . $credit_card_type_map[$type] . '" />';
    }

    return $payment_type_xml;
  }

  /**
   * Returns the map of supported credit cards.
   *
   * @return array
   *   An associative array of all supported credit card types, keyed by their
   *   Paymetric IDs with their payment type name as their values.
   */
  protected function creditCardTypeMap(): array {
    return [
      'AX' => 'american express',
      'DC' => 'diners',
      'DI' => 'discover',
      'JC' => 'jcb',
      'MC' => 'mastercard',
      'VI' => 'visa',
    ];
  }

  /**
   * Return the SOAP xml string that should be included in authorization call.
   *
   * @todo Create an xml template for this.
   *
   * @return string
   *   The xml string.
   */
  protected function getAuthorizationXml($config, $transaction_data): string {
    return '<?xml version="1.0" encoding="UTF-8"?>
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="http://Paymetric/XiPaySoap30/message/">
<SOAP-ENV:Header>
<wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
<wsse:UsernameToken>
<wsse:Username>' . $config['username'] . '</wsse:Username>
<wsse:Password>' . $config['password'] . '</wsse:Password>
</wsse:UsernameToken>
</wsse:Security>
</SOAP-ENV:Header>
<SOAP-ENV:Body>
<ns1:SoapOp>
<ns1:pPacketsIn>
<ns1:count>1</ns1:count>
<ns1:packets>
<ns1:ITransactionHeader>
<ns1:MerchantID>' . $config['routing_mid'] . '</ns1:MerchantID>
<ns1:PacketOperation>1</ns1:PacketOperation>
<ns1:Amount>' . $transaction_data['amount'] . '</ns1:Amount>
<ns1:CardExpirationDate>' . $transaction_data['card_expiration_date'] . '</ns1:CardExpirationDate>
<ns1:CardNumber>' . $transaction_data['remote_id'] . '</ns1:CardNumber>
<ns1:CardDataSource>E</ns1:CardDataSource>
<ns1:CardType>' . $transaction_data['card_type'] . '</ns1:CardType>
<ns1:CurrencyKey>' . $transaction_data['currency_code'] . '</ns1:CurrencyKey>
<ns1:Preauthorized>' . $transaction_data['pre_authorization_amount'] . '</ns1:Preauthorized>
</ns1:ITransactionHeader>
</ns1:packets>
</ns1:pPacketsIn>
</ns1:SoapOp>
</SOAP-ENV:Body>
</SOAP-ENV:Envelope>
';
  }

  /**
   * Re-order the response packet based on the name of the key.
   *
   * From
   *   `[0 => ['Name' => Card Type', 'Value' => 'vi',
   *   'IsToTokenize' => 'false'];`
   * To
   *   `['Card Type' => ['Name' => Card Type', 'Value' => 'vi',
   *   'IsToTokenize' => 'false'];`.
   *
   * @param array $response
   *   The response array.
   *
   * @return array
   *   The array re-ordered based on the name of the key.
   */
  protected function reOrderResponsePacket(array $response): array {
    $reordered_response = [];
    foreach ($response as $data) {
      $reordered_response[$data['Name']] = $data;
    }

    return $reordered_response;
  }

}
