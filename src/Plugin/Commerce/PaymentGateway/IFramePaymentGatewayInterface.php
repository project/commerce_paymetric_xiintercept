<?php

namespace Drupal\commerce_paymetric_xiintercept\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OnsitePaymentGatewayInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsAuthorizationsInterface;

/**
 * Custom interface for the IFramePaymentGateway.
 */
interface IFramePaymentGatewayInterface extends OnsitePaymentGatewayInterface, SupportsAuthorizationsInterface {}
