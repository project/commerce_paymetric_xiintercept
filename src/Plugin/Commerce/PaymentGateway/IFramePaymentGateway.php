<?php

namespace Drupal\commerce_paymetric_xiintercept\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\CreditCard;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Exception\HardDeclineException;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OnsitePaymentGatewayBase;
use Drupal\commerce_paymetric_xiintercept\Api\IFrameClientInterface;
use Drupal\commerce_price\MinorUnitsConverterInterface;
use Drupal\commerce_price\Price;
use Drupal\commerce_price\RounderInterface;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the Paymetric XiIntercept IFrame payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "paymetric_xiintercept_iframe",
 *   label = "Paymetric XiIntercept IFrame",
 *   display_label = "Paymetric XiIntercept IFrame",
 *   forms = {
 *     "add-payment-method" = "Drupal\commerce_paymetric_xiintercept\PluginForm\Onsite\IFramePaymentForm",
 *   },
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "amex", "discover", "mastercard", "visa"
 *   },
 *   js_library = "commerce_paymetric_xiintercept/iframe_form",
 * )
 */
class IFramePaymentGateway extends OnsitePaymentGatewayBase implements IFramePaymentGatewayInterface {

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The Paymetric XiIntercept IFrame API client.
   *
   * @var \Drupal\commerce_paymetric_xiintercept\Api\IFrameClient
   */
  protected $apiClient;

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The rounder.
   *
   * @var \Drupal\commerce_price\RounderInterface
   */
  protected $rounder;

  /**
   * Constructs a new IFramePaymentGateway object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerce_payment\PaymentTypeManager $payment_type_manager
   *   The payment type manager.
   * @param \Drupal\commerce_payment\PaymentMethodTypeManager $payment_method_type_manager
   *   The payment method type manager.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time.
   * @param \Drupal\commerce_price\MinorUnitsConverterInterface $minor_units_converter
   *   The minor units converter.
   * @param \Drupal\commerce_paymetric_xiintercept\Api\IFrameClientInterface $api_client
   *   The Paymetric XiIntercept IFrame API client.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_channel_factory
   *   The logger channel factory.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\commerce_price\RounderInterface $rounder
   *   The rounder.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, PaymentTypeManager $payment_type_manager, PaymentMethodTypeManager $payment_method_type_manager, TimeInterface $time, MinorUnitsConverterInterface $minor_units_converter = NULL, IFrameClientInterface $api_client, LoggerChannelFactoryInterface $logger_channel_factory, MessengerInterface $messenger, RounderInterface $rounder) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager, $payment_method_type_manager, $time, $minor_units_converter);

    $this->apiClient = $api_client;
    $this->logger = $logger_channel_factory->get('commerce_paymetric_xiintercept');
    $this->messenger = $messenger;
    $this->rounder = $rounder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_payment_type'),
      $container->get('plugin.manager.commerce_payment_method_type'),
      $container->get('datetime.time'),
      $container->get('commerce_price.minor_units_converter'),
      $container->get('commerce_paymetric_xiintercept.api_client'),
      $container->get('logger.factory'),
      $container->get('messenger'),
      $container->get('commerce_price.rounder'),
    );
  }

  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration() {
    return [
      'merchant_guid' => '',
      'shared_key' => '',
      'username' => '',
      'password' => '',
      'routing_mid' => '',
      'accepted_credit_cards' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['merchant_guid'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Paymetric Merchant GUID'),
      '#default_value' => $this->configuration['merchant_guid'],
      '#required' => TRUE,
    ];
    $form['shared_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Paymetric Shared Key'),
      '#default_value' => $this->configuration['shared_key'],
      '#required' => TRUE,
    ];
    $form['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('XiPay Username'),
      '#default_value' => $this->configuration['username'],
      '#required' => TRUE,
    ];
    $form['password'] = [
      '#type' => 'textfield',
      '#title' => $this->t('XiPay Password'),
      '#default_value' => $this->configuration['password'],
      '#required' => TRUE,
    ];
    $form['routing_mid'] = [
      '#type' => 'textfield',
      '#title' => $this->t('XiPay Routing MID'),
      '#default_value' => $this->configuration['routing_mid'],
      '#required' => TRUE,
    ];
    $form['accepted_credit_cards'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Accepted Credit Cards'),
      '#description' => $this->t('Select credit card types supported by your merchant account.'),
      '#options' => array_map('ucwords', $this->creditCardTypeMap()),
      '#default_value' => explode(', ', $this->configuration['accepted_credit_cards']),
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    $values = $form_state->getValue($form['#parents']);
    $this->configuration['merchant_guid'] = $values['merchant_guid'];
    $this->configuration['shared_key'] = $values['shared_key'];
    $this->configuration['username'] = $values['username'];
    $this->configuration['password'] = $values['password'];
    $this->configuration['routing_mid'] = $values['routing_mid'];
    $this->configuration['accepted_credit_cards'] = implode(', ', array_filter($values['accepted_credit_cards']));
  }

  /**
   * {@inheritdoc}
   */
  public function createPaymentMethod(
    PaymentMethodInterface $payment_method,
    array $payment_details
  ) {
    // The expected token must always be present.
    $required_keys = [
      'paymetric_access_token',
    ];
    foreach ($required_keys as $required_key) {
      if (empty($payment_details[$required_key])) {
        throw new \InvalidArgumentException(sprintf(
          '$payment_details must contain the %s key.',
          $required_key
        ));
      }
    }

    $plugin = $payment_method->getPaymentGateway()->getPlugin();
    $config = $plugin->getConfiguration();
    // Tokenize the payment in Paymetric and get the tokenized card details.
    $response = $this->apiClient->getResponsePacket($config, $payment_details['paymetric_access_token']);

    $payment_method->card_type = $this->mapCreditCardType(strtoupper($response['Card Type']['Value']));
    $card_number_values = $this->parseCardNumberAndRemoteId($response['Card Number']['Value']);
    $payment_method->card_number = $card_number_values['last_four_digits'];
    $payment_method->card_exp_month = $response['Expiration Month']['Value'];
    $payment_method->card_exp_year = $response['Expiration Year']['Value'];
    $expires = CreditCard::calculateExpirationTimestamp($payment_method->card_exp_month->value, $payment_method->card_exp_year->value);
    $payment_method->setExpiresTime($expires);
    $payment_method->setRemoteId($card_number_values['remote_id']);
    $payment_method->save();
  }

  /**
   * {@inheritdoc}
   */
  public function createPayment(PaymentInterface $payment, $capture = TRUE) {
    $this->assertPaymentState($payment, ['new']);
    $payment_method = $payment->getPaymentMethod();
    $this->assertPaymentMethod($payment_method);

    $plugin = $payment_method->getPaymentGateway()->getPlugin();
    $config = $plugin->getConfiguration();
    // Authorize the payment with XiPay.
    $transaction_data = $this->getTransactionDataForAuthorization($payment);
    $response = $this->apiClient->authorizePayment($config, $transaction_data);

    // Mark the payment as completed.
    $next_state = $capture ? 'completed' : 'authorization';
    $payment->setState($next_state);
    $payment->setRemoteId($payment_method->getRemoteId());
    $payment->save();
    // Set the response on the payment order.
    $payment->getOrder()->setData('xipay_auth_response_for_payment_id_' . $payment->id(), $response)->save();
  }

  /**
   * {@inheritdoc}
   */
  public function capturePayment(PaymentInterface $payment, Price $amount = NULL) {
    // @todo Implement voidPayment() method.
  }

  /**
   * {@inheritdoc}
   */
  public function voidPayment(PaymentInterface $payment) {
    // @todo Implement voidPayment() method.
  }

  /**
   * {@inheritdoc}
   */
  public function deletePaymentMethod(PaymentMethodInterface $payment_method) {
    // @todo Implement deletePaymentMethod() method.
  }

  /**
   * Maps the Paymetric credit card type to a Commerce credit card type.
   *
   * @param string $card_type
   *   The Paymetric credit card type.
   *
   * @return string
   *   The Commerce credit card type.
   */
  protected function mapCreditCardType($card_type) {
    $map = $this->creditCardTypeMap();
    if (!isset($map[$card_type])) {
      throw new HardDeclineException(sprintf('Unsupported credit card type "%s".', $card_type));
    }

    return $map[$card_type];
  }

  /**
   * Returns the map of supported credit cards.
   *
   * @return array
   *   An associative array of all supported credit card types, keyed by their
   *   Paymetric IDs with their Commerce IDs as their values.
   */
  protected function creditCardTypeMap() {
    return [
      'AX' => 'amex',
      'DC' => 'dinersclub',
      'DI' => 'discover',
      'JC' => 'jcb',
      'MC' => 'mastercard',
      'VI' => 'visa',
    ];
  }

  /**
   * Parses and returns the card number and remote ID from the response.
   *
   * @param string $card_value
   *   The card number response. ie. `-E803-1111-RDWAVE6WM0XX7T`, which has the
   *   full remote ID.
   *   This value consists of the token format `-E803`, the last 4 digits of
   *   the credit card `1111`, and a token value `RDWAVE6WM0XX7T`.
   *
   * @return array
   *   An array with the last 4 digits and remote ID.
   */
  protected function parseCardNumberAndRemoteId($card_value) : array {
    preg_match('/^-(\w+)-(\w+)-(\w+)/', $card_value, $matches);
    return ['last_four_digits' => $matches[2], 'remote_id' => $matches[0]];
  }

  /**
   * Composes the transaction data necessary for authorizing a card.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment entity.
   *
   * @return array
   *   An array of details for authorizing the transaction.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  protected function getTransactionDataForAuthorization(PaymentInterface $payment): array {
    $payment_method = $payment->getPaymentMethod();
    $amount = $payment->getAmount();
    $amount = $this->rounder->round($amount);
    $billing_profile = $payment_method->getBillingProfile();
    /** @var \Drupal\address\AddressInterface $billing_address */
    $billing_address = $billing_profile->get('address')->first();
    $card_types = array_flip($this->creditCardTypeMap());
    $expiration_month = strlen($payment_method->card_exp_month->value) < 2 ? ('0' . $payment_method->card_exp_month->value) : $payment_method->card_exp_month->value;
    $expiration_date = $expiration_month . '/' . str_split($payment_method->card_exp_year->value, '2')[1];

    return [
      'remote_id' => $payment_method->getRemoteId(),
      'amount' => $amount->getNumber(),
      'card_type' => $card_types[$payment_method->card_type->value],
      'card_expiration_date' => $expiration_date,
      'currency_code' => $amount->getCurrencyCode(),
      'first_name' => $billing_address->getGivenName(),
      'last_name' => $billing_address->getFamilyName(),
      'address_line_1' => $billing_address->getAddressLine1(),
      'city' => $billing_address->getLocality(),
      'state' => $billing_address->getAdministrativeArea(),
      'country' => $billing_address->getCountryCode(),
      'zip_code' => $billing_address->getPostalCode(),
      'pre_authorization_amount' => $payment_method->card_type->value === 'discover' ? '1' : '0',
    ];
  }

}
