README file for Commerce Paymetric XiIntercept

CONTENTS OF THIS FILE
---------------------
* Introduction
* Requirements
* Installation
* Configuration
* How It Works

INTRODUCTION
------------
This project integrates Paymetric XiIntercept payments into
the Drupal Commerce payment and checkout systems.

REQUIREMENTS
------------
This module requires the following:
* Submodules of Drupal Commerce package (https://drupal.org/project/commerce)
  - Commerce core
  - Commerce Payment (and its dependencies)
* Paymetric XiIntercept Merchant account

INSTALLATION
------------
* This module needs to be installed via Composer, which will download
  the required libraries.
  composer require "drupal/commerce_paymetric_xiintercept"
  https://www.drupal.org/docs/8/extending-drupal-8/installing-modules-composer-dependencies

CONFIGURATION
-------------
* Create a new Paymetric XiIntercept payment gateway.
  Administration > Commerce > Configuration > Payment gateways >
Add payment gateway

HOW IT WORKS
------------

* General considerations:
  - The store owner must have a Paymetric XiIntercept merchant account.
  - Customers should have a valid credit card.

* Payment Terminal
  The store owner can Void, Capture and Refund the Paymetric payments.
