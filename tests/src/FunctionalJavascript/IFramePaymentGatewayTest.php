<?php

namespace Drupal\Tests\commerce_paymetric_xiintercept\FunctionalJavascript;

use Drupal\Core\Url;
use Drupal\Tests\commerce\FunctionalJavascript\CommerceWebDriverTestBase;

/**
 * @coversDefaultClass \Drupal\commerce_paymetric_xiintercept\Plugin\Commerce\PaymentGateway\IFramePaymentGateway
 * @group commerce_paymetric_xiintercept
 */
class IFramePaymentGatewayTest extends CommerceWebDriverTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'commerce_payment',
    'commerce_paymetric_xiintercept',
  ];

  /**
   * {@inheritdoc}
   */
  protected function getAdministratorPermissions() {
    return array_merge([
      'administer commerce_payment_gateway',
    ], parent::getAdministratorPermissions());
  }

  /**
   * Tests using the payment gateway add form.
   */
  public function testPaymentGatewayCreation() {
    $this->drupalGet(Url::fromRoute('entity.commerce_payment_gateway.add_form')->toString());
    $expected_options = [
      'Manual',
      'Paymetric XiIntercept IFrame',
    ];
    $page = $this->getSession()->getPage();
    foreach ($expected_options as $expected_option) {
      $radio_button = $page->findField($expected_option);
      $this->assertNotNull($radio_button);
    }

    $radio_button = $this->getSession()->getPage()->findField('Paymetric XiIntercept IFrame');
    $radio_button->click();
    $this->assertSession()->assertWaitOnAjaxRequest();
    $this->assertSession()->fieldExists('Name');
    $this->assertSession()->fieldExists('Merchant GUID');
    $this->assertSession()->fieldExists('Shared Key');
    $this->assertSession()->pageTextContains('Accepted Credit Cards');

    $page = $this->getSession()->getPage();
    $page->fillField('label', 'Paymetric IFrame Credit Card');
    $page->fillField('configuration[paymetric_xiintercept_iframe][merchant_guid]', 'testMerchantGuid');
    $page->fillField('configuration[paymetric_xiintercept_iframe][shared_key]', 'testSharedKey');
    $page->fillField('configuration[paymetric_xiintercept_iframe][username]', 'testUsername');
    $page->fillField('configuration[paymetric_xiintercept_iframe][password]', 'testPassword');
    $page->fillField('configuration[paymetric_xiintercept_iframe][routing_mid]', 'testRoutingMid');
    $page->checkField('Amex');
    $page->checkField('Discover');
    $page->checkField('Mastercard');
    $page->checkField('Visa');
    $this->submitForm([], 'Save');
    $page->fillField('id', 'paymetric_xiintercept_iframe');
    $this->submitForm([], 'Save');

    $this->drupalGet('admin/commerce/config/payment-gateways');

    $entity_type_manager = $this->container->get('entity_type.manager');
    /** @var \Drupal\commerce_payment\Entity\PaymentGatewayInterface $onsite_payment_gateway */
    $onsite_payment_gateway = $entity_type_manager->getStorage('commerce_payment_gateway')->load('paymetric_xiintercept_iframe');
    $this->assertEquals('testMerchantGuid', $onsite_payment_gateway->getPluginConfiguration()['merchant_guid']);
    $this->assertEquals('testSharedKey', $onsite_payment_gateway->getPluginConfiguration()['shared_key']);
    $this->assertEquals('testUsername', $onsite_payment_gateway->getPluginConfiguration()['username']);
    $this->assertEquals('testPassword', $onsite_payment_gateway->getPluginConfiguration()['password']);
    $this->assertEquals('testRoutingMid', $onsite_payment_gateway->getPluginConfiguration()['routing_mid']);
    $this->assertEquals('Paymetric IFrame Credit Card', $onsite_payment_gateway->label());
    $this->assertEquals('test', $onsite_payment_gateway->getPlugin()->getMode());
    $this->assertEquals('AX, DI, MC, VI', $onsite_payment_gateway->getPluginConfiguration()['accepted_credit_cards']);
  }

}
