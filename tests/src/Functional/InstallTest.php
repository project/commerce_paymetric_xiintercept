<?php

namespace Drupal\Tests\commerce_paymetric_xiintercept\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests module installation.
 *
 * @group commerce_paymetric_xiintercept
 */
class InstallTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Admin user account.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();
    $this->drupalLogin($this->drupalCreateUser(['administer modules']));
  }

  /**
   * Tests module installation.
   */
  public function testInstall() {
    $page = $this->getSession()->getPage();
    $assert_session = $this->assertSession();

    $this->drupalGet(Url::fromRoute('system.modules_list')->toString());
    $page->checkField('modules[commerce_paymetric_xiintercept][enable]');
    $page->pressButton('Install');
    $assert_session->statusCodeEquals(200);
    $assert_session->pageTextContains('Some required modules must be enabled');
    $page->pressButton('Continue');
    $assert_session->statusCodeEquals(200);
    $assert_session->pageTextContains('modules have been enabled: Commerce Paymetric XiIntercept');
  }

}
