<?php

namespace Drupal\Tests\commerce_paymetric_xiintercept\Unit;

use Drupal\commerce_paymetric_xiintercept\Api\IFrameClient;
use Drupal\commerce_paymetric_xiintercept\Api\IFrameClientInterface;
use Drupal\Tests\UnitTestCase;
use GuzzleHttp\ClientInterface;
use Prophecy\Argument;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;
use Psr\Log\LoggerInterface;

/**
 * @coversDefaultClass \Drupal\commerce_paymetric_xiintercept\Api\IFrameClient
 * @group commerce_paymetric_xiintercept
 */
class IFrameApiTest extends UnitTestCase {

  /**
   * @covers ::createAccessToken
   */
  public function testCreateAccessToken() {
    $fixture = __DIR__ . '/../../fixtures/AccessToken.xml';
    $body = $this->prophesize(StreamInterface::class);
    $body->getContents()->willReturn(file_get_contents($fixture));
    $response = $this->prophesize(ResponseInterface::class);
    $response->getBody()->willReturn($body->reveal());

    $client = $this->prophesize(ClientInterface::class);
    $client->request('POST', IFrameClientInterface::ACCESS_TOKEN_URL_DEV, Argument::type('array'))
      ->willReturn($response->reveal());
    $logger = $this->prophesize(LoggerInterface::class);

    $api = new IFrameClient($client->reveal(), $logger->reveal());
    $this->assertEquals('42333-3443-4355-3533-53533', $api->createAccessToken([
      'mode' => 'test',
      'merchant_guid' => '123',
      'shared_key' => 'abc',
      'accepted_credit_cards' => 'AX, DI, MC, VI',
    ]));
  }

  /**
   * @covers ::getResponsePacket
   */
  public function testGetResponsePacket() {
    $fixture = __DIR__ . '/../../fixtures/ResponsePacket.xml';
    $body = $this->prophesize(StreamInterface::class);
    $body->getContents()->willReturn(file_get_contents($fixture));
    $response = $this->prophesize(ResponseInterface::class);
    $response->getBody()->willReturn($body->reveal());

    $client = $this->prophesize(ClientInterface::class);
    $client->request('GET', IFrameClientInterface::IFRAME_RESPONSE_PACKET_URL_DEV, Argument::type('array'))
      ->willReturn($response->reveal());
    $logger = $this->prophesize(LoggerInterface::class);

    $expected_response = $this->getExpectedResponsePacketResponse();
    $api = new IFrameClient($client->reveal(), $logger->reveal());
    $this->assertEquals($expected_response, $api->getResponsePacket([
      'mode' => 'test',
      'merchant_guid' => '123',
      'shared_key' => 'abc',
      'accepted_credit_cards' => 'AX, DI, MC, VI',
    ], '42333-3443-4355-3533-53533'));
  }

  /**
   * @covers ::authorizePayment
   */
  public function testAuthorizePayment() {
    $fixture = __DIR__ . '/../../fixtures/AuthorizationResponse.xml';
    $body = $this->prophesize(StreamInterface::class);
    $body->getContents()->willReturn(file_get_contents($fixture));
    $response = $this->prophesize(ResponseInterface::class);
    $response->getBody()->willReturn($body->reveal());

    $client = $this->prophesize(ClientInterface::class);
    $client->request('POST', IFrameClientInterface::XIPAY_AUTHORIZATION_URL_DEV, Argument::type('array'))
      ->willReturn($response->reveal());
    $logger = $this->prophesize(LoggerInterface::class);

    $api = new IFrameClient($client->reveal(), $logger->reveal());
    $transaction_data = [
      'remote_id' => '-E803-4113-W7B9V4BZPE1D6F',
      'amount' => '5',
      'card_type' => 'VI',
      'card_expiration_date' => '02/23',
      'currency_code' => 'USD',
      'first_name' => 'John',
      'last_name' => 'Smith',
      'address_line_1' => 'Test Address',
      'city' => 'Test City',
      'state' => 'CA',
      'country' => 'US',
      'zip_code' => '10001',
      'pre_authorization_amount' => '0',
    ];
    $expected_response = $this->getExpectedAuthorizationResponse();
    $this->assertEquals($expected_response, $api->authorizePayment([
      'mode' => 'test',
      'merchant_guid' => '123',
      'shared_key' => 'abc',
      'username' => 'testusername',
      'password' => 'testpassword',
      'routing_mid' => 'testRoutingMid',
      'accepted_credit_cards' => 'AX, DI, MC, VI',
    ], $transaction_data));
  }

  /**
   * Returns an array of the expected response after the response packet call.
   *
   * @return array[]
   *   An array of values.
   */
  protected function getExpectedResponsePacketResponse() {
    return [
      'Card Type' => [
        'Name' => 'Card Type',
        'Value' => 'vi',
        'IsToTokenize' => 'false',
      ],
      'Expiration Month' => [
        'Name' => 'Expiration Month',
        'Value' => 1,
        'IsToTokenize' => 'false',
      ],
      'Expiration Year' => [
        'Name' => 'Expiration Year',
        'Value' => '2025',
        'IsToTokenize' => 'false',
      ],
      'Card Number' => [
        'Name' => 'Card Number',
        'Value' => '-E803-4113-W7B9V4BZPE1D6F',
        'IsToTokenize' => 'true',
      ],
      'Card Security Code' => [
        'Name' => 'Card Security Code',
        'Value' => '123',
        'IsToTokenize' => 'false',
      ],
      'Card Holder Name' => [
        'Name' => 'Card Holder Name',
        'Value' => 'John Smith',
        'IsToTokenize' => 'false',
      ],
    ];
  }

  /**
   * Returns an array of the expected response after authorization.
   *
   * @return array[]
   *   An array of values.
   */
  protected function getExpectedAuthorizationResponse(): array {
    return [
      'Amount' => '5.0000',
      'AuthorizationCode' => 'tst690',
      'AuthorizationDate' => '2022-03-08T11:25:50',
      'AuthorizationTime' => '11:25:0050',
      'AuthorizedThroughCartridge' => 'XiPayCartPTSalem.PTSalem.1',
      'BillingDate' => '1899-12-30T00:00:00',
      'CaptureDate' => '1899-12-30T00:00:00',
      'CardDataSource' => 'E',
      'CardExpirationDate' => '05/27',
      'CardNumber' => '-E803-4113-W7B9V4BZPE1D6F',
      'CardType' => 'VI',
      'ChargeAmount' => '0.0000',
      'CreationDate' => '2022-03-08T11:25:50',
      'CurrencyKey' => 'USD',
      'LastModificationDate' => '2022-03-08T11:25:50',
      'MerchantID' => 'USD_IGD_IDG',
      'Message' => '100 Approved - Successfully approved.',
      'ModifiedStatus' => '1',
      'OrderDate' => '1899-12-30T00:00:00',
      'Origin' => 'XiPay',
      'PacketOperation' => '1',
      'ResponseCode' => '100',
      'SettlementAmount' => '5.0000',
      'SettlementDate' => '1899-12-30T00:00:00',
      'ShippingCaptureDate' => '1899-12-30T00:00:00',
      'StatusCode' => '100',
      'StatusTXN' => 'Authorized',
      'TaxLevel1' => '0.0000',
      'TaxLevel2' => '0.0000',
      'TaxLevel3' => '0.0000',
      'TaxLevel4' => '0.0000',
      'TransactionID' => '240139338',
      'TransactionType' => 'Authorization',
      'XIID' => '33432',
      'InfoItems' => [
        'InfoItem' => [
          0 => [
            'Key' => 'CSV_RESULT',
            'Value' => [],
          ],
          1 => [
            'Key' => 'HOST_AUTH',
            'Value' => 'gdgsdsPrgdasdas4zKgdgddsdgsfA=',
          ],
          2 => [
            'Key' => 'IN_CARD_TYPE',
            'Value' => 'VI',
          ],
          3 => [
            'Key' => 'IN_TRANS_AUTHDATETIME',
            'Value' => '220308',
          ],
          4 => [
            'Key' => 'IN_TRANS_DATETIME',
            'Value' => '1646760350',
          ],
          5 => [
            'Key' => 'IN_TRANS_MERCHORDERNUM',
            'Value' => '240139338',
          ],
          6 => [
            'Key' => 'IN_TRANS_TYPE',
            'Value' => '1',
          ],
          7 => [
            'Key' => 'RESPONSECODE',
            'Value' => '100',
          ],
          8 => [
            'Key' => 'TR_CARD_AFFLUENT',
            'Value' => 'N',
          ],
          9 => [
            'Key' => 'TR_CARD_CIDRESPCODE',
            'Value' => [],
          ],
          10 => [
            'Key' => 'TR_CARD_COMMERCIAL',
            'Value' => 'Y',
          ],
          11 => [
            'Key' => 'TR_CARD_DURBIN',
            'Value' => 'N',
          ],
          12 => [
            'Key' => 'TR_CARD_HEALTHCARD',
            'Value' => 'N',
          ],
          13 => [
            'Key' => 'TR_CARD_ISSUERCNTRYCODE2',
            'Value' => 'USA',
          ],
          14 => [
            'Key' => 'TR_CARD_LEVEL3',
            'Value' => 'X',
          ],
          15 => [
            'Key' => 'TR_CARD_PAYROLL',
            'Value' => 'N',
          ],
          16 => [
            'Key' => 'TR_CARD_PINLESSDEBIT',
            'Value' => 'Y',
          ],
          17 => [
            'Key' => 'TR_CARD_PREPAID',
            'Value' => 'X',
          ],
          18 => [
            'Key' => 'TR_CARD_RESPCODE',
            'Value' => '100',
          ],
          19 => [
            'Key' => 'TR_CARD_SIGDEBIT',
            'Value' => 'N',
          ],
          20 => [
            'Key' => 'TR_ECOMM_VBVCAVVRESPCODE',
            'Value' => [],
          ],
        ],
      ],
    ];
  }

}
