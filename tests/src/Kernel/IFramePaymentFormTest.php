<?php

namespace Drupal\Tests\commerce_paymetric_xiintercept\Kernel\Form;

use Drupal\commerce_paymetric_xiintercept\PluginForm\Onsite\IFramePaymentForm;
use Drupal\commerce_store\StoreCreationTrait;
use Drupal\KernelTests\KernelTestBase;

/**
 * @coversDefaultClass \Drupal\commerce_paymetric_xiintercept\PluginForm\Onsite\IFramePaymentForm
 * @group commerce_paymetric_xiintercept
 */
class IFramePaymentFormTest extends KernelTestBase {

  use StoreCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'commerce',
    'commerce_store',
    'commerce_paymetric_xiintercept',
  ];

  /**
   * Test the form exists.
   */
  public function testFormExists() {
    $current_store = $this->container->get('commerce_store.current_store');
    $entity_type_manager = $this->container->get('entity_type.manager');
    $inline_form_manager = $this->container->get('plugin.manager.commerce_inline_form');
    $logger = $this->container->get('logger.channel.commerce_paymetric_xiintercept');
    $api_client = $this->container->get('commerce_paymetric_xiintercept.api_client');
    $messenger = $this->container->get('messenger');

    $form = new IFramePaymentForm(
      $current_store,
      $entity_type_manager,
      $inline_form_manager,
      $logger,
      $api_client,
      $messenger,
    );
    $this->assertNotNull($form);
  }

}
